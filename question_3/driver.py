import pytesseract
import cv2 as cv

from PIL import Image
import requests
import shutil
import numpy as np

from skimage import io
from skimage.color import rgb2gray
from skimage.transform import rotate

from deskew import determine_skew

# image = io.imread('input.png')
# grayscale = rgb2gray(image)
# angle = determine_skew(grayscale)
# rotated = rotate(image, angle, resize=True) * 255
# io.imsave('output.png', rotated.astype(np.uint8))

# pytesseract.pytesseract.tesseract_cmd = r'/usr/local/bin/tesseract'
# image = Image.open(requests.get('https://building-management.publicwork.ntpc.gov.tw/ImageServlet', stream=True).raw)
# image = Image.open(requests.get('http://www.goldenjade.com.tw/captchaCheck/check/showrandimg.php', stream=True).raw)
# img = cv.imread('q3.png', 0)
# img = cv.cvtColor(img, cv.COLOR_BGR2RGB)
# (h, w) = img.shape[:2]
# img = cv.resize(img, (w*2, h*2))
# img = cv.morphologyEx(img, cv.MORPH_CLOSE, None)
# img = cv.threshold(img, 150, 255, cv.THRESH_BINARY)[1]
# img = Image.fromarray(img)
# img.show()
# text = pytesseract.image_to_string(img, config='-l eng --psm 7 --oem 1 -c tessedit_char_whitelist=abcdefghijklmnopqrstuvwxyz0123456789')
# print(text)


def solve_captcha(res):
    pytesseract.pytesseract.tesseract_cmd = r'/usr/local/bin/tesseract'
    if res.status_code == 200:
        with open('q3.png', 'wb') as f:
            res.raw.decode_content = True
            shutil.copyfileobj(res.raw, f)
            f.close()
    img = cv.imread('q3.png', 0)

    row, col = img.shape[:2]
    bottom = img[row-2:row, 0:col]
    mean = cv.mean(bottom)[0]

    bordersize = 10
    img = cv.copyMakeBorder(
        img,
        top=bordersize,
        bottom=bordersize,
        left=bordersize,
        right=bordersize,
        borderType=cv.BORDER_CONSTANT,
        value=[mean, mean, mean]
    )


    Image.fromarray(img).show()
    img = cv.cvtColor(img, cv.COLOR_BGR2RGB)
    (h, w) = img.shape[:2]
    img = cv.resize(img, (w*2, h*2))
    img = cv.morphologyEx(img, cv.MORPH_CLOSE, None)
    img = cv.threshold(img, 150, 255, cv.THRESH_BINARY)[1]
    img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    # img = Image.fromarray(img)

    # Find the contours
    contours, hierarchy = cv.findContours(img, cv.RETR_LIST, cv.CHAIN_APPROX_SIMPLE)
    contours = sorted([(c, cv.boundingRect(c)[0]) for c in contours], key = lambda x: x[1])
    # For each contour, find the bounding rectangle and draw it
    ary = []
    for (cnt, _) in contours:
        x,y,w,h = cv.boundingRect(cnt)
        if w > 15 and h > 15 and w < 80 and h < 80:
            ary.append((x,y,w,h))

    ary.sort(key=lambda x: x[0])


    # fig = plt.figure()
    captcha_result = ''
    for id, (x,y,w,h) in enumerate(ary):
        roi = img[y:y + h, x:x + w]
        thresh = roi.copy()
        # Image.fromarray(thresh).show()
        captcha_result = captcha_result + pytesseract.image_to_string(thresh, config='-l eng --psm 7 --oem 1 -c tessedit_char_whitelist=abcdefghijklmnopqrstuvwxyz0123456789').strip()
        # cv.rectangle(img,(x,y),(x+w,y+h),(0,255,0),2)
        # cv.rectangle(thresh_color,(x,y),(x+w,y+h),(0,255,0),2)
    print(captcha_result)
    # Finally show the image
    # cv.imshow('img',img)
    # img = Image.fromarray(img)
    # cv.imshow('res',thresh_color)
    # cv.destroyAllWindows()
    # img.show()

    # image, contours, hierarchy = cv.findContours(img.copy(), cv.RETR_TREE,cv.CHAIN_APPROX_SIMPLE)
    # img.show()

    # pytesseract.pytesseract.tesseract_cmd = r'/usr/local/bin/tesseract'
    # captcha_result = pytesseract.image_to_string(img, config='-l eng --psm 7 --oem 1')
    # print(captcha_result)
    # return captcha_result


if __name__ == '__main__':
    solve_captcha(requests.get('http://www.goldenjade.com.tw/captchaCheck/check/showrandimg.php', stream=True, verify=False))
