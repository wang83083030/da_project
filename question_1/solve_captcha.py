import pytesseract

from PIL import Image
import requests
import shutil

# Pass in the same session request
def solve_captcha(res):
    # Check if the request is succeeded
    if res.status_code == 200:
        with open('temp.png', 'wb') as f:
            res.raw.decode_content = True
            shutil.copyfileobj(res.raw, f)
            f.close()
    image = Image.open('temp.png')

    # Specify the path to tesseract
    pytesseract.pytesseract.tesseract_cmd = r'/usr/local/bin/tesseract'

    # Use tesseract to find digits in the image
    captcha_result = pytesseract.image_to_string(image, config = 'digits')

    return captcha_result


if __name__ == '__main__':
    solve_captcha(requests.get('https://building-management.publicwork.ntpc.gov.tw/ImageServlet', stream=True, verify=False))
