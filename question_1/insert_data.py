import pandas as pd
import mysql.connector as mysql
from mysql.connector import Error

building_license = pd.read_csv('result.csv', index_col=False, delimiter = ',')
building_license = building_license.astype(object).where(pd.notnull(building_license), None)

try:
    conn = mysql.connect(host='localhost', user='root')#give ur username, password
    if conn.is_connected():
        cursor = conn.cursor()
        cursor.execute("CREATE DATABASE cathayDB")
        print("Database is created")
except Error as e:
    print("Error while connecting to MySQL", e)

try:
    conn = mysql.connect(host='localhost', database='cathayDB', user='root')
    if conn.is_connected():
        cursor = conn.cursor()
        cursor.execute("select database();")
        record = cursor.fetchone()
        print("You're connected to database: ", record)
        cursor.execute('DROP TABLE IF EXISTS building_license;')
        print('Creating table....')
# in the below line please pass the create table statement which you want #to create
        cursor.execute("CREATE TABLE building_license (district varchar(255),road varchar(255),use_permit varchar(255),build_permit varchar(255),builder varchar(255),designer varchar(255),address varchar(255),issue_date varchar(255))")
        print("Table is created....")
        #loop through the data frame
        for i, row in building_license.iterrows():
            #here %S means string values
            sql = "INSERT INTO cathayDB.building_license VALUES (%s,%s,%s,%s,%s,%s,%s,%s)"
            cursor.execute(sql, tuple(row))
            print("Record inserted")
            # the connection is not auto committed by default, so we must commit to save our changes
            conn.commit()
except Error as e:
    print("Error while connecting to MySQL", e)
