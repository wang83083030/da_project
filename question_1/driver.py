import get_scraping_data
import solve_captcha

from bs4 import BeautifulSoup
import requests
import csv
import os.path
import pandas as pd

# Specify columns of the output
column_name_list = ['district', 'road', 'use_permit', 'build_permit', 'builder', 'designer', 'address', 'issue_date']

# Check if result.csv file does not exist or is empty,
# create the file and write header columns to the file
if not os.path.isfile('result.csv') or os.stat('result.csv').st_size == 0:
    with open('result.csv', 'a+', encoding='utf-8') as f:
       writer = csv.writer(f)
       writer.writerow(column_name_list)


# ------------------------------------------------------------------------------
# scraping by each road
# ------------------------------------------------------------------------------

# Get the scraping data from get_scraping_data.py
scraping_data = get_scraping_data.get_district_code_road_list()

for district_code_road in scraping_data:
    df = pd.read_csv('result.csv')

    # Check if the iteration road has already been proccessed
    completed_record = set(df['district'] + df['road'])
    if district_code_road[0] + district_code_road[2] in completed_record:
        continue

    # Print progress in terminal
    print('crawling: ', district_code_road[0], district_code_road[2], end='', flush=True)

    flush_data = []
    attempt = 0

    # While loop attempt 10 times
    while attempt < 10:
        try:
            # Initiate a new session for sending the captcha answer
            rs = requests.session()
            res = rs.get('https://building-management.publicwork.ntpc.gov.tw/ImageServlet', stream=True)
            captcha = solve_captcha.solve_captcha(res)

            # Prepare the form data for POST request
            form_data = {
                'PagePT': '1',
                'A2': '3',
                'D1': district_code_road[1],
                'D3': district_code_road[2],
                'Z1': captcha
            }

            # Change encoding to BIG5
            form_data = {k: v.encode("BIG5") for k,v in form_data.items()}

            result = rs.post('https://building-management.publicwork.ntpc.gov.tw/bm_list.jsp', data=form_data)

            soup = BeautifulSoup(result.content, 'html.parser')

            # Get the return record counts for this iteration
            return_count = int(soup.find(id="RecCT").text)

            break
        except:
            attempt = attempt + 1

    # While loop continue if still has records to proccess
    while int(return_count) > 0:
        this_page_data = soup.find(id='DataBlock').find_all('tr')

        # For loop every record for the page
        for tr in this_page_data:

            # Make sure the record is not empty
            if tr.find_all('td')[0].find('a').text:
                # Append the result to flush_data
                flush_data.append(
                    [district_code_road[0],
                    district_code_road[2],
                    tr.find_all('td')[0].find('a').text,
                    tr.find_all('td')[1].text,
                    tr.find_all('td')[2].text,
                    tr.find_all('td')[3].text,
                    tr.find_all('td')[4].text,
                    tr.find_all('td')[5].text]
                )
            return_count = return_count - 1

        # Increment the page number
        form_data['PagePT'] = str(int(form_data['PagePT']) + 1)
        result = rs.post('https://building-management.publicwork.ntpc.gov.tw/bm_list.jsp', data=form_data)
        soup = BeautifulSoup(result.content, 'html.parser')

    # Write the data for all results in this road to result.csv
    with open('result.csv', 'a+', encoding='utf-8') as f:
       writer = csv.writer(f)
       writer.writerows(flush_data)

    print(' - done')

# ------------------------------------------------------------------------------
# scraping by each district
# ------------------------------------------------------------------------------

# scraping_data = get_scraping_data.get_district_code_dict()
#
# for district, code in scraping_data.items():
#     attempt = 0
#     while attempt < 10:
#         try:
#             rs = requests.session()
#             res = rs.get('https://building-management.publicwork.ntpc.gov.tw/ImageServlet', stream=True)
#             captcha = solve_captcha.solve_captcha(res)
#             form_data = {
#                 'PagePT': '1',
#                 'A2': '3',
#                 'D1': code,
#                 'Z1': captcha
#             }
#             form_data = {k: v.encode("BIG5") for k,v in form_data.items()}
#             result = rs.post('https://building-management.publicwork.ntpc.gov.tw/bm_list.jsp', data=form_data)
#             soup = BeautifulSoup(result.content, 'html.parser')
#             return_count = int(soup.find(id="RecCT").text)
#             break
#         except:
#             attempt = attempt + 1
#
#     print('crawling: ', district, end='', flush=True)
#
#     while int(return_count) > 0:
#         this_page_data = soup.find(id='DataBlock').find_all('tr')
#         for tr in this_page_data:
#             if tr.find_all('td')[0].find('a').text:
#                 final_data.append([district, tr.find_all('td')[0].find('a').text, tr.find_all('td')[4].text, tr.find_all('td')[5].text])
#             return_count = return_count - 1
#
#         form_data['PagePT'] = str(int(form_data['PagePT']) + 1)
#         result = rs.post('https://building-management.publicwork.ntpc.gov.tw/bm_list.jsp', data=form_data)
#         soup = BeautifulSoup(result.content, 'html.parser')
#     print(' - done')
