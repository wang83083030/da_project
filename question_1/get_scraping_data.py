import requests
import json
import csv
from bs4 import BeautifulSoup
import re

def get_district_code_dict():
    # Get the district and code mapping from the url
    response = requests.get('https://building-management.publicwork.ntpc.gov.tw/_setData.jsp?rt=D1')

    soup = BeautifulSoup(response.content, 'html.parser')

    district_map_code = {}

    for item in soup.find_all('div', class_='div_w_85'):
        district = re.findall(r'([\u4e00-\u9fff]+)', item['onclick'])[0]
        code = re.findall('[0-9]+', item['onclick'])[0]
        district_map_code[district] = code

    return district_map_code

def get_district_road_list():
    district_road_list = []
    # taiwan_all_road.csv is from "https://data.gov.tw/dataset/35321"
    # We are able to download every road in Taiwan all in once,
    # or get it from the OpenAPI (currently available for 2000 rows only)
    with open('taiwan_all_road.csv', 'r') as csvfile:
        datareader = csv.reader(csvfile)
        # Get only New Taipei City roads in the file
        for row in datareader:
            if row[0] == "新北市":
                district_road_list.append([row[1], row[2]])

    return district_road_list

def get_district_code_road_list():
    district_road_list = []
    district_map_code = {}
    district_road_list = get_district_road_list()
    district_map_code = get_district_code_dict()

    district_code_road_list = []
    for i in range(len(district_road_list)):
        district = district_road_list[i][0]
        code = 0
        if district in district_map_code:
            code = district_map_code[district]
        road = district_road_list[i][1]

        district_code_road_list.append([district, code, road])

    return district_code_road_list

if __name__ == '__main__':
    get_district_code_dict()
