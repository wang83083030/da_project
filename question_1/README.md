-- In order to solve the captch, Tesseract is needed in this project

-- Use homebrew to install Tesseract

$bash brew install tesseract


-- If you are using other OS or don't have homebrew,
-- please see this page
-- (https://www.pyimagesearch.com/2017/07/03/installing-tesseract-for-ocr/)
-- for detail

-- MySQL is also needed in the project

$bash brew install mysql


-- If you are using other OS or don't have homebrew,
-- please see this page
-- https://dev.mysql.com/doc/mysql-installation-excerpt/5.7/en/
-- for detail


-- Kindly just run the code through driver.py

$bash python driver.py


-- the crawler will take several hours, but the final result has been stored
-- in result.csv
