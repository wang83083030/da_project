import pandas as pd
import mysql.connector as mysql
from mysql.connector import Error

try:
    conn = mysql.connect(host='localhost', user='root')#give ur username, password
    if conn.is_connected():
        cursor = conn.cursor()
except Error as e:
    print("Error while connecting to MySQL", e)

# Execute query

# Q1
print("Q1:")
sql = "SELECT district, COUNT(DISTINCT use_permit) AS use_permit_count FROM cathayDB.building_license GROUP BY district ORDER BY use_permit_count DESC"
cursor.execute(sql)
# Fetch all the records
result = cursor.fetchall()
for i in result:
    print(i)
print()
# Ans: The most permits district is 三重區; the least permits district is 烏來區
