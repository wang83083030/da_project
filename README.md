-- Below bash commands are running under Mac OS

-- cd into this da_project folder

$bash cd da_prokect


-- activate Python virtual environment

$bash source venv/bin/activate


-- install all the Python packages

$bash pip3 install -r requirement.txt


-- cd into question_1, question_2, or question_3 folder
-- to run the corresponding codes or to see the detail steps
